# -*- coding: UTF-8 -*-
'''
Created on 2014-02-04
Updated on 2014-10-09 for Python 3.4
@author: Johnny Tsheke at UQAM
Une autre solution possible en utilisant else avec la boucle for
'''

liste=[32, -12, 7, 9,20, -7]
somme=0
for elem in liste:
    somme =somme+elem
    print(elem)
else:
    print("somme = ",somme)