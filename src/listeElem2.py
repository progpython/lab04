# -*- coding: UTF-8 -*-
'''
Created on 2014-02-04
Updated on 2014-10-09 for Python 3.4
@author: Johnny Tsheke at UQAM
Une autre solution utilisant while avec else
'''

liste=[]
elem=0
while elem>=0:
    elem=input("Entrer un nombre svp\n")
    elem=eval(elem)
    liste.append(elem)
else:
    liste.sort()
    print(liste)

    
